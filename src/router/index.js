/**
 * @description router全局配置，如有必要可分文件抽离，其中asyncRoutes只有在intelligence模式下才会用到，pro版只支持remixIcon图标，具体配置请查看vip群文档
 */
import Vue from 'vue'
import VueRouter from 'vue-router'
import Layout from '@/vab/layouts'
import {
  publicPath,
  routerMode
} from '@/config'

Vue.use(VueRouter)
export const constantRoutes = [{
    path: "/redirect",
    component: Layout,
    hidden: true,
    children: [{
      path: "/redirect/:path(.*)",
      component: () => import("@/views/template/redirect/index")
    }]
  },
  {
    path: '/login',
    component: () => import('@/views/login'),
    meta: {
      hidden: true,
    },
  },
  {
    path: '/wx_login',
    component: () => import('@/views/login/wx_login'),
    meta: {
      hidden: true,
    },
  },
  {
    path: '/wx_login_success',
    component: () => import('@/views/login/wx_login_success'),
    meta: {
      hidden: true,
    },
  },
  {
    path: '/',
    name: 'Root',
    component: Layout,
    meta: {
      title: '首页',
      icon: 'er-icon-index',
      breadcrumbHidden: true,
    },
    children: [{
      path: 'index',
      name: 'Index',
      component: () => import('@/views/index/index'),
      meta: {
        title: '首页',
        icon: 'er-icon-index',
        noClosable: true,
      },
    },{
      path: 'reportscan',
      name: 'Reportscan',
      component: () => import('@/views/coder/reportscan'),
      meta: {
        hidden: true,
        title: '报表预览',
        icon: 'er-icon-menu'
      },
    }, {
      path: 'msg',
      name: 'msg',
      component: () => import('@/views/coder/msg/msg'),
      meta: {
        hidden: true,
        title: '消息中心',
      }
    },  ],
  },
]

//开发者路由
export const coderRoutes = [{
  path: '/coder',
  component: Layout,
  name: 'Coder',
  hidden: false,
  meta: {
    title: '开发配置',
    icon: 'er-icon-coder'
  },
  children: [{
      path: 'data',
      name: 'Data',
      component: () => import('@/views/coder/data'),
      meta: {
        title: '元数据管理',
        icon: 'er-icon-data'
      }
    },
    {
      path: 'menu',
      name: 'Menu',
      component: () => import('@/views/coder/menu'),
      meta: {
        title: '菜单管理',
        icon: 'er-icon-menu'
      },
    },
    {
      path: 'permission',
      name: 'Permission',
      component: () => import('@/views/coder/permission'),
      meta: {
        title: '权限列表',
        icon: 'er-icon-permission'
      },
    },
    {
      path: 'dicts',
      name: 'Dicts',
      component: () => import('@/views/coder/dicts'),
      meta: {
        title: '字典管理',
        icon: 'er-icon-dicts'
      },
    },
    {
      path: 'form_making',
      name: 'Form_making',
      component: () => import('@/views/coder/form_making'),
      meta: {
        title: '表单设计',
        icon: 'er-icon-dicts'
      },
    },
    {
      path: 'quartz',
      name: 'Quartz',
      component: () => import('@/views/coder/quartz'),
      meta: {
        title: '定时任务',
        icon: 'er-icon-quartz'
      },
    },
    {
      path: 'report',
      name: 'Report',
      component: () => import('@/views/coder/report'),
      meta: {
        title: '报表开发',
        icon: 'er-icon-menu'
      },
    },
    {
      path: 'print',
      name: 'Print',
      component: () => import('@/views/coder/print'),
      meta: {
        title: '打印模板',
        icon: 'er-icon-menu'
      },
    },
    {
      path: 'reportscan',
      name: 'Reportscan',
      component: () => import('@/views/coder/reportscan'),
      meta: {
        hidden: true,
        title: '报表预览',
        icon: 'er-icon-menu'
      },
    },
    {
      path: 'company',
      name: 'Company',
      component: () => import('@/views/coder/company'),
      meta: {
        title: '企业管理',
        icon: 'er-icon-company'
      },
    }
  ]
}]

export const asyncRoutes = [

]

const router = createRouter()

function fatteningRoutes(routes) {
  return routes.flatMap((route) => {
    return route.children ? fatteningRoutes(route.children) : route
  })
}

export function resetRouter(routes = constantRoutes) {
  routes.map((route) => {
    if (route.children) {
      route.children = fatteningRoutes(route.children)
    }
  })
  router.matcher = createRouter(routes).matcher
}

function createRouter(routes = constantRoutes) {
  return new VueRouter({
    base: publicPath,
    mode: routerMode,
    scrollBehavior: () => ({
      y: 0,
    }),
    routes: routes,
  })
}

const originalPush = VueRouter.prototype.push
VueRouter.prototype.push = function push(location) {
  return originalPush.call(this, location).catch((err) => err)
}

export default router
