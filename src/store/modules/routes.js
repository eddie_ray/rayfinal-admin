/**
 * @description 路由拦截状态管理，目前两种模式：all模式与intelligence模式，其中partialRoutes是菜单暂未使用
 */
import Vue from 'vue'
import { asyncRoutes, constantRoutes, coderRoutes, resetRouter } from '@/router'
import { getList } from '@/api/router'
import { convertRouter, filterRoutes } from '@/utils/routes'
import { authentication, rolesControl } from '@/config'
import { isArray } from '@/utils/validate'
import store from '@/store'

var _this = this

const state = () => ({
  routes: [],
  activeName: '',
})
const getters = {
  routes: (state) => state.routes,
  activeName: (state) => state.activeName,
}
const mutations = {
  /**
   * @description 多模式设置路由
   * @param {*} state
   * @param {*} routes
   */
  setRoutes(state, routes) {
    state.routes = routes
  },
  /**
   * @description 修改Meta
   * @param {*} state
   * @param options
   */
  changeMenuMeta(state, options) {
    function handleRoutes(routes) {
      return routes.map((route) => {
        if (route.name === options.name) Object.assign(route.meta, options.meta)
        if (route.children && route.children.length)
          route.children = handleRoutes(route.children)
        return route
      })
    }
    state.routes = handleRoutes(state.routes)
  },
  /**
   * @description 修改 activeName
   * @param {*} state
   * @param activeName 当前激活菜单
   */
  changeActiveName(state, activeName) {
    state.activeName = activeName
  },
}

const actions = {

  /**
   * 把从后端查询的菜单数据拼装成路由格式的数据
   * @param data 后端返回的菜单数据
   */
  generaMenu(routes,data) {
    data.forEach(item => {
      const menu = {
        path: item.path,
        name: item.no,
        component: item.component === '#' ? 'Layout' : item.component === '##' ? '@/views/template/empty' : `@/views${item.component}`,
        hidden: item.is_hide === 1, // 状态为1的隐藏
        children: [],
        meta: {
          title:item.title,
          icon:item.icon,
          query:item
        }
      }
      if (item.children) {
        actions.generaMenu(menu.children, item.children)
      }
      routes.push(menu)
    })
    return routes
  },

  /**
   * @description 多模式设置路由
   * @param {*} { commit }
   * @param mode
   * @returns
   */
  async setRoutes({ commit }, mode = 'none') {
    // 默认前端路由
    let routes = [...asyncRoutes]
    // 设置游客路由关闭路由拦截(不需要可以删除)
    const control = mode === 'visit' ? false : rolesControl
    // 设置后端路由(不需要可以删除)
    if (authentication === 'all') {
      const {
        menus,
      } = await getList(store.getters['user/userinfo'].userid)
      const list = actions.generaMenu([],menus);
      if(list.length>0){
        if (!isArray(list))
          Vue.prototype.$baseMessage(
            '路由格式返回有误！',
            'error',
            'vab-hey-message-error'
          )
        if (list[list.length - 1].path !== '*')
          list.push({ path: '/404', redirect: '/404', meta: { hidden: true } })
        routes = convertRouter(list)
      }
    }
    routes = Object.assign([], store.getters['user/userinfo'].is_coder?coderRoutes:{}).concat(routes)
    var accessRoutes = []
    // 根据权限和rolesControl过滤路由
    accessRoutes = filterRoutes([...constantRoutes,...routes], control)
    // 设置菜单所需路由
    commit('setRoutes', JSON.parse(JSON.stringify(accessRoutes)))
    // 根据可访问路由重置Vue Router
    await resetRouter(accessRoutes)
  },
  /**
   * @description 修改Route Meta
   * @param {*} { commit }
   * @param options
   */
  changeMenuMeta({ commit }, options = {}) {
    commit('changeMenuMeta', options)
  },
  /**
   * @description 修改 activeName
   * @param {*} { commit }
   * @param activeName 当前激活菜单
   */
  changeActiveName({ commit }, activeName) {
    commit('changeActiveName', activeName)
  },
}
export default { state, getters, mutations, actions }
