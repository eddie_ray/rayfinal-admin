import store from '@/store'

function auth(permission_no){
  if (store.getters['user/userinfo'].permissions.indexOf(permission_no) !== -1){
    return true
  }
  return false
}

export default{
  auth
}
