import Vue from 'vue'
import VXETable from 'vxe-table'

import EditDownTextarea from './EditDownTextarea.vue';
Vue.component('EditDownTextarea', EditDownTextarea);

import CellSelector from './CellSelector.vue';
Vue.component('CellSelector', CellSelector);

import FormSelector from './FormSelector.vue';
Vue.component('FormSelector', FormSelector);

//下拉输入框
VXETable.renderer.add('EditDownTextarea', {
  autofocus: '.vxe-input--inner',
  renderEdit (h, renderOpts, params) {
    return [
      h('edit-down-textarea', {
          attrs: {
        	  params: params
          }
      })
    ]
  }
})

//前端字段格式化
VXETable.formats.add('format', ({ cellValue,row }, formatter) => {
	return eval(formatter);
})

// select显示模板
VXETable.renderer.add('cell-select', {
  renderDefault (h, renderOpts, params) {
    let { row, column } = params
    let { options } = renderOpts
    if(options!=null){
      for(var i=0;i<options.length;i++){
        if(options[i].value==row[column.property]){
          return options[i].label;
        }
      }
    }
    return row[column.property];
  }
});

// switch显示模板
VXETable.renderer.add('cell-switch', {
  renderDefault (h, renderOpts, params) {
    let { row, column } = params
    if(row[column.property]){
    	return '是'
    }else{
    	return '否'
    }
  }
})

VXETable.renderer.add('CellSelector', {
  renderDefault (h, renderOpts, params) {
    //let { row, column } = params
    if(params.column.cellRender.props.is_formatter){
      return [
        h('CellSelector', {
            attrs: {
          	  params: params
            }
        })
      ]
    }else{
      return params.row[params.column.property+'_show_value']
    }
  }
})

VXETable.renderer.add('FormSelector', {
  renderItemContent (h, renderOpts, params) {
    //let { row, column } = params
    params.props = renderOpts.props
    return [
      h('FormSelector', {
          attrs: {
            params: params
          }
      })
    ]
  }
})
