import request from '@/utils/request'

export function getList(userid) {
  return request({
    url: '/menu/menus',
    method: 'get',
    params:{userid:userid}
  })
}
