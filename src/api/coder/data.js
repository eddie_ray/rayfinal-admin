import request from '@/utils/request'

/**
 * 获取所有元对象
 */
export function getObjects(data_object_no) {
  return request({
    url: '/data/getObjects',
    method: 'get',
    params:{data_object_no:data_object_no}
  })
}

/**
 * 获取数据库数据源
 */
export function getDataSource() {
  return request({
    url: '/data/getDataSource',
    method: 'get'
  })
}

/**
 * 获取数据源下所有数据库表
 */
export function getTables(data_source) {
  return request({
    url: '/data/getTables',
    method: 'get',
    params:{data_source:data_source}
  })
}

/**
 * 行内修改元对象
 */
export function changeObject(data) {
  return request({
    url: '/data/changeObject',
    method: 'post',
    params: data
  })
}

/**
 * 新增元对象
 */
export function newObject(data) {
  return request({
    url: '/data/newObject',
    method: 'post',
    params: data
  })
}

/**
 * 获取元数据下的字段和按钮
 */
export function getFieldsAndButtons(no) {
  return request({
    url: '/data/getFieldsAndButtons',
    method: 'post',
    params:{no:no}
  })
}

/**
 * 修改元对象
 */
export function objectEdit_(row) {
  return request({
    url: '/data/updateObject',
    method: 'post',
    params:row
  })
}

/**
 * 复制元对象
 */
export function objectCopy_(row) {
  return request({
    url: '/data/copyObject',
    method: 'post',
    params:row
  })
}

/**
 * 迁移元对象
 */
export function objectSql(row) {
  return request({
    url: '/data/exportModel',
    method: 'post',
    params:{id:row.id}
  })
}

/**
 * 删除元对象
 */
export function objectDel_(no) {
  return request({
    url: '/data/del',
    method: 'post',
    params:{no:no}
  })
}

/**
 * 新建虚拟字段
 */
export function fieldSubmit(form) {
  return request({
    url: '/data/newField',
    method: 'post',
    params:form
  })
}

/**
 * 删除字段
 */
export function delField(row) {
  return request({
    url: '/data/delField',
    method: 'post',
    params:row
  })
}

/**
 * 新增按钮
 */
export function buttonNew_(form) {
  return request({
    url: '/data/newButton',
    method: 'post',
    params:form
  })
}

/**
 * 删除按钮
 */
export function buttonDel_(row) {
  return request({
    url: '/data/delButton',
    method: 'post',
    params:row
  })
}

/**
 * 字段更改
 */
export function fieldChange(row) {
  return request({
    url: '/data/changeFiled',
    method: 'post',
    params:row
  })
}

/**
 * 按钮更改
 */
export function buttonChange(row) {
  return request({
    url: '/data/changeButton',
    method: 'post',
    params:row
  })
}
