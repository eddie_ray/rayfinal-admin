import request from '@/utils/request'

var path = "/msg"


/**
 * 获取我的消息
 */
export function getData(params) {
  return request({
    url: path + '/getData',
    method: 'get',
    params: params
  })
}

/**
 * 工作台获取我的消息列表
 */
export function getMsgList(is_read,size) {
  return request({
    url: path + '/getMsgList',
    method: 'get',
    params: {is_read:is_read,size:size}
  })
}

/**
 * 根据消息ID获取消息详情
 */
export function getMsgByNo(no) {
  return request({
    url: path + '/getMsgByNo',
    method: 'get',
    params: {no:no}
  })
}


/**
 * 点击详情已读
 */
export function detailRead(id) {
  return request({
    url: path + '/detailRead',
    method: 'post',
    params: {id:id}
  })
}

/**
 * 列表勾选已读
 */
export function read(rows) {
  return request({
    url: path + '/read',
    method: 'post',
    params: {rows:rows}
  })
}
