import request from '@/utils/request'

var path = "/print"


/**
 * 根据类型获取打印模板列表
 */
export function getList(type) {
  return request({
    url: path + '/getList',
    method: 'get',
    params: {type:type}
  })
}

/**
 * 根据编号获取打印模板
 */
export function getTemplate(no) {
  return request({
    url: path + '/getTemplate',
    method: 'get',
    params: {no:no}
  })
}