import request from '@/utils/request'

var path = "/report"


/**
 * 获取所有报表
 */
export function getReportList() {
  return request({
    url: path + '/getReportList',
    method: 'get'
  })
}

/**
 * 修改报表名称
 */
export function updateName(id,name) {
  return request({
    url: path + '/updateName',
    method: 'post',
    params: {id:id,name:name}
  })
}

/**
 * 新增报表分类
 */
export function add(name) {
  return request({
    url: path + '/add',
    method: 'post',
    params: {name:name}
  })
}

/**
 * 新增报表
 */
export function addReport(parent_id,name) {
  return request({
    url: path + '/addReport',
    method: 'post',
    params: {parent_id:parent_id,name:name}
  })
}

/**
 * 保存报表
 */
export function save(form) {
  return request({
    url: path + '/save',
    method: 'post',
    params: form
  })
}

/**
 * 删除报表
 */
export function delReport(id) {
  return request({
    url: path + '/delReport',
    method: 'post',
    params: {id:id}
  })
}


/**
 * 报表预览-获取列
 */
export function getColumns(id) {
  return request({
    url: path + '/getColumns',
    method: 'post',
    params: {id:id}
  })
}

/**
 * 报表预览-获取数据
 */
export function getList(id,params) {
  return request({
    url: path + '/getList',
    method: 'post',
    params: {id:id,params:params}
  })
}

/**
 * 报表预览-导出数据
 */
export function reportExport(id,params) {
  return request({
    url: path + '/reportExport',
    method: 'post',
    params: {id:id,params:params}
  })
}
