import request from '@/utils/request'

var path = "/menu"
/**
 * 获取菜单
 */
export function getTreeMenu() {
  return request({
    url: path+'/getTreeMenu',
    method: 'get',
  })
}

/**
 * 新增或修改菜单
 */
export function menuAddOrEdit(form) {
  return request({
    url: path+'/menuAddOrEdit',
    method: 'post',
    params:form
  })
}

/**
 * 删除菜单
 */
export function menuDel(id,region) {
  return request({
    url: path+'/delMenu',
    method: 'post',
    params:{id:id,region:region}
  })
}
