import request from '@/utils/request'

var path = "/wx"

/**
 * 获取微信用户信息
 */
export function getUser(code) {
  return request({
    url: path + '/getUser',
    method: 'get',
    params:{code:code}
  })
}

