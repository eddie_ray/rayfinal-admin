import request from '@/utils/request'

export async function login(data) {
  return request({
    url: '/login',
    method: 'post',
    params: data,
  })
}

export async function wx_login(data) {
  return request({
    url: '/wx_login',
    method: 'post',
    params: data,
  })
}

export function getUserInfo() {
  return request({
    url: '/getUserinfo',
    method: 'get',
  })
}

export function logout() {
  return request({
    url: '/logout',
    method: 'get',
  })
}

export function updatePwd(params) {
  return request({
    url: '/updatePass',
    method: 'post',
    params:params
  })
}

export function register(data) {
  return request({
    url: '/register',
    method: 'post',
    data,
  })
}
