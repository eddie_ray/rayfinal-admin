import request from '@/utils/request'

var path = "/template"
/**
 * 获取菜单
 */
export function getMenuInfo(id) {
  return request({
    url: path + '/getMenuInfo',
    method: 'get',
    params: {
      id: id
    }
  })
}

/**
 * 根据元数据获取字段列表
 */
export function getColumns(data_object_no) {
  return request({
    url: path + '/getColumns',
    method: 'get',
    params: {
      data_object_no: data_object_no
    }
  })
}

/**
 * 获取数据
 */
export function getData(data_object_no,
  queryForm,
  currentPage,
  pageSize) {
  return request({
    url: path + '/getData',
    method: 'get',
    params: {
      data_object_no: data_object_no,
      queryForm: queryForm,
      currentPage: currentPage,
      pageSize: pageSize
    }
  })
}

/**
 * 获取弹窗选择器数据
 */
export function getSelectorData(item) {
  var typeConfig = JSON.parse(item.type_config)
  return request({
    url: typeConfig.type === 'method' ? typeConfig.method : path + '/getSelectorData',
    method: 'get',
    params: item
  })
}

/**
 * 新增数据
 */
export function new_(params) {
  return request({
    url: path + '/new_',
    method: 'post',
    params: params
  })
}

/**
 * 修改数据
 */
export function edit_(params) {
  return request({
    url: path + '/edit_',
    method: 'post',
    params: params
  })
}

/**
 * 删除数据
 */
export function del_(id, data_object_no) {
  return request({
    url: path + '/del_',
    method: 'post',
    params: {
      id: id,
      data_object_no: data_object_no
    }
  })
}

/**
 * 获取树形结构数据
 */
export function getTree(data_object_no,
  tree_parent_no,
  tree_no,
  tree_field) {
  return request({
    url: path + '/getTree',
    method: 'get',
    params: {
      data_object_no: data_object_no,
      tree_parent_no: tree_parent_no,
      tree_no: tree_no,
      tree_field: tree_field
    }
  })
}

/**
 * 万能POST请求
 */
export function post_(url,params) {
  return request({
    url: url,
    method: 'post',
    params: params
  })
}
