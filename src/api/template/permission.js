import request from '@/utils/request'

var path = "/permissions"


/**
 * 获取权限列表
 */
export function getList(role_no) {
  return request({
    url: path + '/getList',
    method: 'get',
    params: {
      role_no: role_no
    }
  })
}

/**
 * 保存权限
 */
export function update(role_no,permission_nos) {
  return request({
    url: path + '/update',
    method: 'post',
    params: {
      role_no: role_no,
      permission_nos: permission_nos
    }
  })
}

/**
 * 删除角色
 */
export function del(row) {
  return request({
    url: '/role/del',
    method: 'post',
    params: row
  })
}

/**
 * 获取角色列表
 */
export function getRoleList() {
  return request({
    url: '/role/getRoleList',
    method: 'get'
  })
}

/**
 * 根据用户id获取角色列表
 */
export function getRoleListByUserid(params) {
  return request({
    url: '/role/getRoleListByUserid',
    method: 'get',
    params:params
  })
}

/**
 * 分配用户角色列表
 */
export function updateUserRole(roles,userid) {
  return request({
    url: '/role/updateUserRole',
    method: 'post',
    params:{roles:roles,userid:userid}
  })
}

/**
 * 根据角色编号获取用户列表
 */
export function getUserByRole(role_no) {
  return request({
    url: '/role/getUserByRole',
    method: 'post',
    params:{role_no:role_no}
  })
}

/**
 * 根据角色编号获取用户列表
 */
export function setRoleUser(role_no,user_ids) {
  return request({
    url: '/role/setRoleUser',
    method: 'post',
    params:{role_no:role_no,user_ids:user_ids}
  })
}
