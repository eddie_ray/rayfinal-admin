import request from '@/utils/request'

var path = "/file"


/**
 * 获取文件列表
 */
export function getFileList(ids) {
  return request({
    url: path + '/getFileList',
    method: 'get',
    params: {
      ids: ids
    }
  })
}

/**
 * 上传文件
 */
export function confirmUpload(params) {
  return request({
    url: path + '/confirmUpload',
    method: 'post',
    params: params
  })
}
