import request from '@/utils/request'

var path = "/common"

/**
 * 导出table数据
 */
export function export_(params) {
  return request({
    url: path + '/export',
    method: 'post',
    params: params
  })
}

/**
 * 更新字典
 */
export function updateDicts() {
  return request({
    url: path + '/updateDicts',
    method: 'post'
  })
}