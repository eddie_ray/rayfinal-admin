import request from '@/utils/request'

var path = "/userSelector"
/**
 * 获取指定可选用户列表
 */
export function getUsersByIds(userids) {
  return request({
    url: path+'/getUsersByIds',
    method: 'get',
    params:{userids:userids}
  })
}

/**
 * 根据关键字搜索用户信息
 */
export function getUserByKeywords(keywords) {
  return request({
    url: path+'/getUserByKeywords',
    method: 'get',
    params:{keywords:keywords}
  })
}

/**
 * 获取用户所在的所有部门
 */
export function getMydpts() {
  return request({
    url: path+'/getMydpts',
    method: 'get'
  })
}

/**
 * 根据部门编号获取当前部门用户列表和子部门列表
 */
export function getDptsByNo(no) {
  return request({
    url: path+'/getDptsByNo',
    method: 'get',
    params:{no:no}
  })
}

/**
 * 获取角色列表
 */
export function getRoles() {
  return request({
    url: path+'/getRoles',
    method: 'get'
  })
}

/**
 * 根据角色编号获取角色下用户列表
 */
export function getUserByRole(role_no) {
  return request({
    url: path+'/getUserByRole',
    method: 'get',
    params:{role_no:role_no}
  })
}
