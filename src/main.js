import Vue from 'vue'
import App from './App'
import i18n from './i18n'
import store from './store'
import router from './router'
import '@/vab'

import VXETable from 'vxe-table'
import 'vxe-table/lib/style.css'
import '@/components/vxe-render'
import '@/styles/color.css' //颜色主题
import '@/styles/vxe-table.scss'
VXETable.setup({
  zIndex: 2000,
  modal: {
    minWidth: 340,
    minHeight: 200,
  },
})
Vue.use(VXETable)

import VXETablePluginElement from 'vxe-table-plugin-element'
import 'vxe-table-plugin-element/dist/style.css'
VXETable.use(VXETablePluginElement)

import ViewUI from 'view-design'
import 'view-design/dist/styles/iview.css'
Vue.use(ViewUI)

import UserSelector from '@/components/userSelector/index'
Vue.prototype.$UserSelector = UserSelector

// 全局引入 WebSocket 通讯组件
import '@/utils/websocket'

import ButtonAuth from '@/components/auth/buttonAuth'
Vue.prototype.$ButtonAuth = ButtonAuth

//引入 iconfont
import "@/icon/iconfont.js";

//全局注册组件
import ErIcon from "@/components/iconfont/iconfont.vue";
Vue.component("er-icon", ErIcon);
//import '@/icon/iconfont/iconfont.css'//iconfont图标

//FormMaking组件
import FormMaking from 'form-making'
import 'form-making/dist/FormMaking.css'
Vue.use(FormMaking, {lang: 'zh-CN', i18n})

//ray打印设计组件
import Raycto from 'raycto'
import 'raycto/default.css'
Vue.use(Raycto)

Vue.config.productionTip = false
new Vue({
  el: '#app',
  i18n,
  store,
  router,
  render: (h) => h(App),
})
