# Rayfinal-admin

#### 介绍

基于Jfinal + Vue 2.0实现的前后端分离快速开发平台

#### 技术堆栈

前端：[Vue2.0](https://cn.vuejs.org/) [Element-ui](https://element.eleme.cn/) [vxe-table v3+](https://vxetable.cn/v3) [vue-admin-beautiful](https://vue-admin-beautiful.com/)

后端：[Jfinal](https://jfinal.com/)

特别鸣谢：[eova](https://www.eova.cn/) 架构理念来源

[开发文档](http://doc.palmdoit.com/)

[在线示例](http://system.palmdoit.com:8888/rayfinal/)


#### 安装教程

1. clone 项目至本地

   ```
   git clone https://gitee.com/eddie_ray/rayfinal-admin.git
   
   npm install
   ```

2. 修改后端服务地址 net.config.js

3. ```
   npm run serve
   ```

#### 后端服务地址

[rayfinal-server](https://gitee.com/eddie_ray/rayfinal-server)

任何技术及授权问题请联系微信：Raycto
