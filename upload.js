let client = require('scp2');
const ora = require('ora');
const chalk = require('chalk');
const spinner = ora(chalk.green('正在发布到服务器...'));
spinner.start();

client.scp('./dist/', {    // 本地打包后，项目包的相对路径，默认为./dist
  "host": '171.92.12.19', // 云服务器的IP地址
  "port": '8877',            // 云服务器端口， 一般为22
  "username": 'Administrator',       // 云服务器用户名，linux系统一般为root
  "password": 'A123456z',     // 云服务器密码，若忘记了，可以去云服务器主页重置密码
  "path": '/D/palmdoit/test'   // 项目包上传到云服务器的目标位置，根据nginx配置决定
}, err =>{
  spinner.stop();
  if (!err) {
    console.log("项目发布完毕!")
  } else {
    console.log("err", err)
  }
})
